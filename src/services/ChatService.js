import { Message } from "../models/Message"


/**
 * Service pour les messages
 */
class ChatService{



    /**
     * ajout du message dans l'historique
     */
    addMessage = (messages , pseudo, texte) => {
        // console.log("histserv :" + {messages})
        // //const messages = {...hist}
        // messages[`message-${Date.now()}`] = {msg : { pseudo : pseudo , texte : texte, date: Date.now()}}
        // console.log(messages[`message-${Date.now()}`])
        // return {...messages}

        let mess = Message
        //const messages = {...hist}
        mess = [...messages, {msg : { pseudo : pseudo , texte : texte, date: Date.now()}}]
        return mess
    }

    // /**
    //  * recup tout les messages
    //  */
    // getMessages = () => {
    //     return this.messages;
    // }

}

/**
 * Creation d'un singleton
 */
export const chatService = Object.freeze(new ChatService());