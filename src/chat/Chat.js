import React, { Component, Fragment } from 'react';
import {chatService} from '../services/ChatService'
import { Message } from '../models/Message';
import Formulaire from '../components/Formulaire'

class Chat extends Component {
  constructor(props){
    super(props)
    this.state = {
      message : {
        texte: "aaaaa",
        pseudo: "Thomas",
        date : "",
        partie: "test"
      },
      historique : []
    }
  }

  handleChange = (event) => {
    const msg = Message
    msg.pseudo = "thomas"
    msg.texte = event.target.value
    this.setState({message : msg})
    //console.log(this.state.message.map(key => message = this.state.message[key].message))
  }

  handleSubmit = (event) => {
    event.preventDefault()
    console.log("stateMsg : " + this.state.message.pseudo + " " +this.state.message.texte)
    //met à jour l'historique avec le nouveau message
    const hist = chatService.addMessage([...this.state.historique],this.state.message.pseudo,this.state.message.texte)
    this.setState({historique : hist})
    console.log("histo : " + this.state.historique + "test : " + hist)
  }


  render() {
    
    return (
      <Fragment>
        <Formulaire 
        histo = {[this.state.historique]}
        submit = {this.handleSubmit}
        change = {this.handleChange}/>
        <textarea className="chat" value={this.state.historique}/>
      </Fragment>
    );
  }
}

export default Chat;