import React from 'react';
import MessageList from './MessageList';

const Formulaire = ({histo, submit, change}) => {
    return (
        <form onSubmit ={submit}>
          <input type= 'text' className="message" onChange={change}/>
          <button>clique</button>
          <MessageList messages = {[histo]}/> 
        </form>
    );
};

export default Formulaire;