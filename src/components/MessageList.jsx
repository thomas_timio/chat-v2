import React from 'react';
import MessageRender from './MessageRender';


//const messages = [{pseudo : "thomas" , message : "test"},{pseudo : "jean" , message : "testtetet"},{pseudo : "thomasdsdsdsd" , message : "fdtgfdsest"}]

/**
 * Fonction qui renvoie un component message pour chaque message apparaissant dans la liste
 * @param  {...any} messages les messages sont des objets dans un tableau
 */
const MessageList = (...messages) => {
    return (
        messages.map((element, key) => <MessageRender element = {[element]}/>)
    );
};

export default MessageList;