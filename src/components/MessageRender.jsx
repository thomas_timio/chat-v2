import React from 'react';


/**
 * Pour chaque objet reçu, renvoie un component message
 * @param {*} props 
 */
const MessageRender = (msg) => {
    console.log("render : " + msg)
    return (
        <div className = "message">
            {msg.pseudo} {msg.texte}
        </div>
    );
};

export default MessageRender;
