﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using chatBack.Models;
using chatBack.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace chatBack.Controllers
{
    [Route("messages")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        private IMessageService service;

        public MessageController(IMessageService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<Message> FindAll()
        {
            return this.service.FindAll();
        }
    }
}