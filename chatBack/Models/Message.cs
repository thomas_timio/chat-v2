﻿using System;
using System.Collections.Generic;

namespace chatBack.Models
{
    public partial class Message
    {
        public int Id { get; set; }
        public string Texte { get; set; }
        public string Pseudo { get; set; }
        public DateTime Date { get; set; }
        public int? IdPartie { get; set; }
    }
}
