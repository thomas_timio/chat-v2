﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace chatBack.Repositories
{
    using Models;
    public interface IMessageRepository
    {
        public IEnumerable<Message> FindAll();
    }
}
