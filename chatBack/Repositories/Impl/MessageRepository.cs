﻿using chatBack.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace chatBack.Repositories.Impl
{
    public class MessageRepository : IMessageRepository
    {
        private List<Message> messages = new List<Message>();
        public IEnumerable<Message> FindAll()
        {
            return this.messages;
        }
    }
}
