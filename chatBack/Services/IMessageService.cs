﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace chatBack.Services
{
    using Models;
    public interface IMessageService
    {
        public IEnumerable<Message> FindAll();
    }
}
