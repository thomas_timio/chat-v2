﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace chatBack.Services.Impl
{
    using chatBack.Repositories;
    using Models;
    public class MessageService :IMessageService
    {
        private List<Message> messages = new List<Message>();
        private IMessageRepository repo;

        public MessageService(IMessageRepository repo)
        {
            this.repo = repo;
        }
        public IEnumerable<Message> FindAll()
        {
            return this.repo.FindAll();
        }
    }
}
